# BTI360 Docker Kata Demo Image

This image is based on the library `node:6-alpine` image, adding `curl` and other
packages as necessary to support training exercises

## Usage

```bash
docker pull registry.gitlab.com/bti360-kata/node:6-alpine
docker run registry.gitlab.com/bti360-kata/node:6-alpine
```
